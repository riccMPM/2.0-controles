package com.riccmp.controleskotln

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        butOk.setOnClickListener(this)
        butCancelar.setOnClickListener(this)


        llenarSpinnerListado()
    }

    override fun onClick(v: View) {

        when (v.id) {

            R.id.butOk -> {
                val numero: String = eteInfomacion.getText().toString()
                Toast.makeText(this, "Valor del texto: $numero", Toast.LENGTH_SHORT).show()

                //Cambiar Imagen
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iviLogo.setImageDrawable(resources.getDrawable(R.drawable.apple, applicationContext.theme))
                } else {
                    iviLogo.setImageDrawable(resources.getDrawable(R.drawable.apple))
                }


            }
            else -> Toast.makeText(this, "Boton Cancelar Apretado", Toast.LENGTH_SHORT).show()

        }


    }


    private fun llenarSpinnerListado() {
        val lstMeses: MutableList<String?> = ArrayList()
        lstMeses.add("Enero")
        lstMeses.add("Febrero")
        lstMeses.add("Marzo")
        lstMeses.add("Abril")
        lstMeses.add("Mayo")
        lstMeses.add("Junio")
        lstMeses.add("Julio")
        lstMeses.add("Agosto")
        lstMeses.add("Septiembre")
        lstMeses.add("Octubre")
        lstMeses.add("Noviembre")
        lstMeses.add("Diciembre")


        // Creating adapter for spinner
        val dataAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, lstMeses)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spiMeses.adapter = dataAdapter
    }

    private fun llenarSpinnerArray() {

        // Creating adapter for spinner
        val dataAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.array_meses))


        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spiMeses.adapter = dataAdapter
    }
}
